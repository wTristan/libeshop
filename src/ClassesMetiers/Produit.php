<?php

namespace Eshop\ClassesMetiers;
use Eshop\Db\DBA;

    class Produit{
        private $idProduit;
        private $libelle;
        private $description;
        private $prix;
        private $image;
        private $categorie;

        public function getCategorie() {
            return $this->categorie;
        }
        
        /**
         * @return mixed
         */
        public function getIdProduit()
        {
            return $this->idProduit;
        }

        /**
         * @param mixed $idProduit
         */
        public function setIdProduit($idProduit)
        {
            $this->idProduit = $idProduit;
        }

        /**
         * @return mixed
         */
        public function getLibelle()
        {
            return $this->libelle;
        }

        /**
         * @param mixed $libelle
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;
        }

        /**
         * @return mixed
         */
        public function getDescription()
        {
            return $this->description;
        }

        /**
         * @param mixed $description
         */
        public function setDescription($description)
        {
            $this->description = $description;
        }

        /**
         * @return mixed
         */
        public function getPrix()
        {
            return $this->prix;
        }

        /**
         * @param mixed $prix
         */
        public function setPrix($prix)
        {
            $this->prix = $prix;
        }

        /**
         * @return mixed
         */
        public function getImage()
        {
            return $this->image;
        }

        /**
         * @param mixed $image
         */
        public function setImage($image)
        {
            $this->image = $image;
        }


        private static $select = "select * from produit";
        private static $selectById ="select * from produit where idProduit = :idProduit";
        private static $insert = "insert into produit(idProduit, libelle,description,image,prix,idCategorie) values (:idProduit, :libelle,:description,:image,:prix ,:idCategorie)";
        private static $update = "update produit set libelle=:libelle,description=:description,image=:image,prix:=prix where idProduit=:idProduit, idCategorie=:idCategorie where idProduit=:idProduit";
        private static $delete ="delete from produit where idProduit = :idProduit";
        private static $selectByIdCategorie = "SELECT * FROM produit WHERE idProduit = :idProduit";

        public static function fetchAll(){
            $collectionProduit = array();
            $dba = new DBA();
            $pdo = $dba->getPDO();
            $pdoStatement = $pdo->query(Produit::$select);
            $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
            foreach ($recordSet as $record){
                $collectionProduit[] = Produit::arrayToProduit($record);
            }
            return $collectionProduit;
        }

        public static function fetch($idProduit) {
            $produit = null ;
            $dba = new DBA();
            $pdo = $dba->getPDO();
            $pdoStatement = $pdo->prepare(Produit::$selectById);
            $pdoStatement->bindParam(":idProduit", $idProduit);
            $pdoStatement->execute();
            $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
            if($record==true){
                $produit = Produit::arrayToProduit($record);
            }
            return $produit;
        }
        public function save(){
            if ($this->idProduit == null){
                $this->insert();
            }
            else{
                $this->update();
            }
        }
        public function insert(){
            $dba = new DBA();
            $pdo = $dba->getPDO();
            $i = "SELECT idCategorie FROM categorie ORDER BY idCategorie DESC LIMIT 1";
            $prep = $pdo->prepare($i);
            $prep->execute();
            $resultat = $prep->fetch();
            $i = $resultat[0];
            $pdoStatement = $pdo->prepare(Produit::$insert);
            $pdoStatement->bindParam(":idProduit", $i);
            $pdoStatement->bindParam(":libelle", $this->libelle);
            $pdoStatement->bindParam(":description",$this->description);
            $pdoStatement->bindParam(":image", $this->image);
            $pdoStatement->bindParam(":prix", $this->prix);
            if ($this->categorie != null) {
                $idCategorie = $this->categorie->getIdCategorie();
            }
            $pdoStatement->bindParam(":idCategorie",$idCategorie);
            $pdoStatement->execute();
            $this->idProduit = $pdo->lastInsertId();
        }
        private function update() {
            $dba = new DBA();
            $pdo = $dba->getPDO();
            $pdoStatement = $pdo->prepare(Produit::$update);
            $pdoStatement->bindParam(":idProduit", $this->idProduit);
            $pdoStatement->bindParam(":libelle", $this->libelle);
            $pdoStatement->bindParam(":description",$this->description);
            $pdoStatement->bindParam(":image", $this->image);
            $pdoStatement->bindParam(":prix", $this->prix);
            if ($this->categorie != null) {
                $idCategorie = $this->categorie->getIdCategorie();
            }
            $pdoStatement->bindParam(":idCategorie",$idCategorie);
            $pdoStatement->execute();
        }
        
        public function setCategorie(Categorie $categorie = null) {
            $this->categorie = $categorie;
            if ($categorie != null) {
                $categorie->addProduit($this);
            }
        }
        
        public function compareTo(Produit $produit) {
            return $produit->idProduit == $this->idProduit;
        }
        
        public function delete() {
            $pdo = (new DBA())->getPDO();
            $pdoStatement = $pdo->prepare(Produit::$delete);
            $pdoStatement->bindParam("idProduit", $this->idProduit);
            $resultat = $pdoStatement->execute();
            $nblignesAffectees = $pdoStatement->rowCount();
            if ($nblignesAffectees == 1) {
                $this->getCategorie()->removeProduit($this);
                $this->idProduit = null;
            }
            return $resultat;
        }
        
        private static function arrayToProduit(Array $array, Categorie $categorie = null) {
            $p = new Produit();
            $p->idProduit = $array["idProduit"];
            $p->libelle = $array["libelle"];
            $p->description = $array["description"];
            $p->image = $array["image"];
            $p->prix = $array["prix"];
            if ($categorie == null) {
                if ($array["idCategorie"] != null) {
                    $p->categorie = Categorie::fetch($array["idCategorie"]);
                } else {
                    $p->categorie = null;
                }
            } else {
                $p->categorie = $categorie;
            }
            return $p;
        }
        
        public static function fetchAllByCategorie(Categorie $categorie) {
            $idCategorie = $categorie->getIdCategorie();
            $collectionProduit = array();
            $pdo = (new DBA())->getPDO();
            $pdoStatement = $pdo->prepare(Produit::$selectByIdCategorie);
            $pdoStatement->bindParam(":idCategorie", $idCategorie);
            $pdoStatement->execute();
            $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
            foreach ($recordSet as $record) {
                $collectionProduit[] = Produit::arrayToProduit($record, $categorie);
        }
        return $collectionProduit;
    }




}